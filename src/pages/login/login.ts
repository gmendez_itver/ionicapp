import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams,Platform} from 'ionic-angular';
import { FormBuilder,FormGroup, Validators} from '@angular/forms';

import {DataService} from '../../lib/data.service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  error: any;
  username: string ="";
  password: string ="";
  forma: FormGroup;

  public mensaje:string ="";

  constructor(public  navCtrl: NavController, 
    		      public  navParams: NavParams,
    		      public  formBuilder: FormBuilder,
    		      public  view: ViewController,
              public  platform: Platform,              
              private data:DataService) {

  	  this.forma = this._createForm();

      this.data.clearSession();
      this.mensaje = "";
  }

  private _createForm(){
    	return this.formBuilder.group({
    		username: ['',Validators.required],
    		password: ['',Validators.required],
    	});
  }

  login(){
    var self = this;

  	this.username = this.forma.value.username;
  	this.password = this.forma.value.password;
    this.mensaje = this.username+" / "+this.password
  	console.log("usuario: "+this.username+" / password: "+this.password);

    this.data.login(this.username,this.password).then(res =>{ self.dismiss() })
                                                .catch(error => this.error = error);    
  }

  dismiss() {
      let _data = { 'username': this.username,
                    'password':this.password,
                    'nombre':this.data.nombre,
                    token:this.data.token,
                    id_usuario:this.data.id_usuario };

      this.view.dismiss(_data);
  }

}
