import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { File,DirectoryEntry } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { ViewChild } from '@angular/core';
import { Slides,LoadingController, Loading,ToastController } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { UploadPage }  from '../upload/upload';
import { DataService } from '../../lib/data.service';

//declare var cordova: any;

/**
 * Generated class for the GaleriaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @Component({
  selector: 'page-galeria',
  templateUrl: 'galeria.html',
})
export class GaleriaPage {

    @ViewChild(Slides) slides:Slides;

    loading: Loading;
    private file: File;

    latitude:number=0;
    longitude:number=0;
    id_usuario="";

    debugstring:string="Debug: ";

    images:string[]=[];

    currentImage:number=-1;

    pathStorage:string="";

    dirFotos:string="fotos"; // directorio donde guardar las fotos

    options= {pagination:true,initialSlide: 0,direction: 'horizontal',speed: 300};

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public platform:Platform,
                private filePath: FilePath,
                private photoViewer: PhotoViewer,
                private transfer: Transfer,
                public toastCtrl: ToastController,
                private loadingCtrl: LoadingController,
                private data: DataService) {
        
        this.latitude =  navParams.get("latitude");
        this.longitude = navParams.get("longitude");
        this.id_usuario = this.data.id_usuario;

        this.file = new File();

        if (this.platform.is('android')) {
           this.pathStorage = this.file.dataDirectory;
        } else {
           this.pathStorage = this.file.dataDirectory+'files/';
        }
        
        this.getFilesList(this.dirFotos);       
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad GaleriaPage');
      this.slides.update();
    }

    ionViewWillEnter(){
      // carpeta_fotos, es el nombre que le quieran dar 
      // a la carpeta donde se guardaran las fotos
     this.slides.update();       
    }

    doFileList(dirResolved,dirName){
        if (this.images.length==0){
            this.currentImage = -1;
        }
        this.file.getDirectory(dirResolved,dirName,{create:true})
                      .then((dirEntry:DirectoryEntry)=>{
                              var directoryReader = dirEntry.createReader();
                              directoryReader.readEntries((entries)=>{
                                        console.log("Leyendo directorio: "+dirResolved);
                                        this.debugstring+="Leyendo directorio: "+dirResolved;
                                        var i;

                                        this.images.length = 0;

                                        for (i=0; i<entries.length; i++) {
                                            this.currentImage = 0;
                                            console.log(entries[i].name);
                                            this.images.push(entries[i].name);
                                        }
                              },
                              (error)=>{
                                      this.debugstring = "Falla en la lectura del directorio";
                                      console.log("Falla en lectura del contenido del directorio: " + error.code+ " / "+error.message);
                              });
                            })
                      .catch((error)=>{
                            this.debugstring = "Falla en la obtencion del directorio";
                            console.log("Falla en la obtencion del directorio: " + error.code+" / "+error.message);
                      });
    }

    getFilesList(dirName){
         console.log("dataDirectory: "+this.pathStorage);
         this.debugstring = "dataDirectory: "+this.pathStorage;


         if (this.platform.is('android')) {
               this.filePath.resolveNativePath(this.pathStorage)
                      .then(filePath => {
                              this.file.resolveDirectoryUrl(filePath)
                                          .then((dirResolved)=>{
                                                this.doFileList(dirResolved,dirName);
                                          })
                                          .catch(error=>{
                                            this.debugstring = "Falla en la resolucion del directorio";
                                            console.log("Falla en la resolucion del directorio: " + error.code+" / "+error.message);
                                          });
                            })
          } else {
            this.file.resolveDirectoryUrl(this.pathStorage)
                        .then((dirResolved)=>{
                              this.doFileList(dirResolved,dirName);
                        })
                        .catch(error=>{
                          this.debugstring = "Falla en la resolucion del directorio";
                          console.log("Falla en la resolucion del directorio: " + error.code+" / "+error.message);
                        });
          }
    }

    public pathForImage(img) {
      if (img === null) {
        return '';
      } else {
        return this.pathStorage+this.dirFotos+'/'+img;
      }
    }

    goToUpload() {
        this.navCtrl.push(UploadPage, {id_usuario: this.data.id_usuario,
                                       token: this.data.token,
                                       latitude: this.latitude,
                                       longitude: this.longitude
                                      });
    }

    deleteImage(fileName){

        if(this.images.length<=0){
           return;
        }

        this.debugstring += "Borrando uno, ";

        var n = -1;

        if (!fileName){
            n = this.slides.getActiveIndex();
            fileName = this.images[n];
        }

        this.file.removeFile(this.pathStorage+this.dirFotos,fileName);
        this.images.slice(n,1);

        this.debugstring += "> Ruta: "+this.pathStorage+" fileName "+fileName;
        console.log("Removiendo un archivo: "+this.pathStorage+this.dirFotos+" fileName "+fileName);

        this.getFilesList(this.dirFotos);

        this.slides.update();
        this.slides.slideTo(n-1);
    }

    public deleteAllImages(){
        var i=0;
        var fileName = "";
        var n = this.images.length;


        this.debugstring += "Borrando todo, ";

        for(i=0; i<n; i++){
          fileName = this.images[i];
          this.file.removeFile(this.pathStorage+this.dirFotos,fileName);
          console.log("Removiendo archivo: "+this.pathStorage+this.dirFotos+"/"+fileName);
        }
        this.getFilesList(this.dirFotos);
        this.slides.update();
    }

    public uploadAll(){
        var i=0;
        var fileName = "";
        var n = this.images.length;

        this.debugstring += "Cargando todo, ";

        this.loading = this.loadingCtrl.create({
          content: 'Enviando...',
          duration: 2000
        });
        this.loading.present();

        for(i=0; i<n; i++){
          fileName = this.images[i];
          this.debugstring += fileName+","
          this.uploadImage(fileName);
        }

        this.loading.dismissAll();
        this.presentToast('Imagen enviada.');
    }

    presentImage(myImage) {
        var mImagePath = this.pathForImage(myImage);
        this.photoViewer.show(mImagePath);
    }

    private presentToast(text) {
        let toast = this.toastCtrl.create({
          message: text,
          duration: 3000,
          position: 'top'
        });
        toast.present();
    }

    public uploadImage(filename) {
      // Destination URL
      var url = "http://ws.misitio.com/upload";

      // File for Upload
      var targetPath = this.pathForImage(filename);


      var options = {
          fileKey: "file",
          fileName: filename,
          chunkedMode: false,
          httpMethod: "POST",
          mimeType: "multipart/form-data",

          params : { 'filename': filename,
                     'id_usuario': this.id_usuario,                     
                     'token':this.data.token,
                     'latitude':this.latitude,
                     'longitude':this.longitude,
                  },

          headers : {'filename': filename,
                     'id_usuario': this.id_usuario,                     
                     'x-access-token':this.data.token,
                     'latitude':this.latitude,
                     'longitude':this.longitude,
                  }

      };

      const fileTransfer: TransferObject = this.transfer.create();


      // Use the FileTransfer to upload the image
      fileTransfer.upload(targetPath, url, options).then(data => {

      }, err => {
          this.loading.dismissAll();
          this.presentToast('Error al intentar enviar la imagen.');
      });
    }

}
