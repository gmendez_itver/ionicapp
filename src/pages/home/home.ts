import { Component } from '@angular/core';
import { NavController, ModalController, Platform} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { DataService } from '../../lib/data.service';

import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  public latitude:number=0;
  public longitude:number=0;

  public latLngOrigen:string = "";
  public latLngDestino:string = "";

  constructor(public navCtrl: NavController,
  			  public platform: Platform,
  	 		  private data:DataService,  	 		 
  	 		  private geolocation: Geolocation,
  			  public modalCtrl:ModalController) {
  }

  ionViewDidLoad(){
  	  this.setGeoLocation();
  	  if (!this.data.isLoggedIn()){  	  	 
          this.logout();
      }
  }

  logout(): void {  	  

      let login = this.modalCtrl.create(LoginPage,{},{showBackdrop:false,enableBackdropDismiss:false});

      login.onDidDismiss(data => {

      });

      login.present();
  }  

   public setGeoLocation(){
		var self = this;

	    var options = {
	        enableHighAccuracy: true,
	        timeout:5000,
	        maximumAge: 3600
	    };

		console.log("Iniciando geolocalizacion ...");
	    this.platform.ready().then((readySource)=>{    
			this.geolocation.getCurrentPosition(options).then((resp) => {
			       self.latitude = resp.coords.latitude;
			       self.longitude = resp.coords.longitude;               

			  let watch = this.geolocation.watchPosition();
			  watch.subscribe((data) => {          	 
			     self.latitude =  data.coords.latitude;
			     self.longitude = data.coords.longitude;
			     console.log("Geolocalizacion establecida ...");
			  }, (error)=>{console.log(error)});
			}).catch((error) => {
			  console.log('Error obteniendo posicion geografica', error);
			});
		}).catch((error)=>{console.log(error)});
    }  

}
