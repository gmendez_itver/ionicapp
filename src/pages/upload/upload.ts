import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ActionSheetController, ToastController, Platform, LoadingController, Loading } from 'ionic-angular';

import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { Camera } from '@ionic-native/camera';

//declare var cordova: any;
/**
 * Generated class for the UploadPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-upload',
  templateUrl: 'upload.html',
})
export class UploadPage {

      token:string=null;
      id_usuario:number;
      lastImage: string = null;
      loading: Loading;
      latitude:number;
      longitude:number;
      nota:string;
      debugstring:string="";
      file:File=null;
      pathStorage:string="";
      basePath:string="";
      dirFotos:string="fotos";

      constructor(public navCtrl: NavController,
                  public navParams: NavParams,
                  private camera: Camera,
                  private filePath: FilePath,
                  private transfer: Transfer,
                  public actionSheetCtrl: ActionSheetController,
                  public toastCtrl: ToastController,
                  public platform: Platform,
                  public loadingCtrl: LoadingController) {

          this.file = new File();

          this.id_usuario = navParams.get("id_usuario");
          this.token      = navParams.get("token");
          this.latitude   = navParams.get("latitude");
          this.longitude  = navParams.get("longitude");

          this.nota="";

          
          if (this.platform.is('android')) {
               this.pathStorage = this.file.dataDirectory;
          } else {
               this.pathStorage = this.file.dataDirectory+'files/';
          }
          
          this.basePath = this.pathStorage.substr(0, this.pathStorage.lastIndexOf('/files/') + 1);

      }

      ionViewDidLoad() {
        console.log('ionViewDidLoad UploadPage');
      }

      public presentActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
          title: 'Selecione la Imagen',
          buttons: [
            {
              text: 'Usar la Camara',
              handler: () => {
                this.takePicture(this.camera.PictureSourceType.CAMERA,this.dirFotos);
              }
            },
            {
              text: 'Subir desde la Galeria',
              handler: () => {
                this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY,this.dirFotos);
              }
            },
            {
              text: 'Cancelar',
              role: 'cancel'
            }
          ]
        });
        actionSheet.present();
      }

      public takePicture(sourceType,dirName) {
        // Create options for the Camera Dialog
        var options = {
          quality: 50,
          sourceType: sourceType,
          saveToPhotoAlbum: false,
          targetWidth: 600,
          targetHeight: 600,
          correctOrientation: true,
          cameraDirection: 0
        };

        console.log("Directorio de almacenamiento: "+this.pathStorage);
        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
          // Special handling for Android library
          this.debugstring += "imagePath: "+imagePath;

          console.log("Imagen tomada: "+imagePath);
          if (this.platform.ready()){
              if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
                this.filePath.resolveNativePath(imagePath)
                  .then(filePath => {
                    let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));

                    this.debugstring+='# filePath: '+filePath;
                    this.debugstring+='# correctPath: '+correctPath;

                    this.copyFileToLocalDir(correctPath, currentName, this.createFileName(),dirName);

                  });
              } else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

                this.debugstring+='# correctPath: '+correctPath;

                this.copyFileToLocalDir(correctPath, currentName, this.createFileName(),dirName);
              }
          }
        }, (err) => {
          this.presentToast('Seleccion de foto cancelado.');
        });


    }

    // Create a new name for the image
    private createFileName() {
        var d = new Date(),
        n = d.getTime(),
        newFileName =  n + ".jpg";
        return newFileName;
    }

    // Copy the image to a local folder
    private copyFileToLocalDir(namePath, currentName, newFileName, dirName) {

        this.debugstring="Checando directorio: ";
        console.log("Checando directorio: "+this.basePath+ " si tiene el directorio files");

        this.file.checkDir(this.basePath,"files")
                    .then((success)=>{
                        this.debugstring+="-- Directorio 'files' existente --";
                        console.log("-- Directorio 'files' existente ");
                        this.prepareCopy(this.pathStorage,namePath, currentName, newFileName, dirName);
                    })
                    .catch((error)=>{
                      var replace=false;
                      this.file.createDir(this.basePath,"files",replace)
                             .then((sucess)=>{
                                  this.debugstring+="-- Directorio 'files' creado --";                                  
                                  this.prepareCopy(this.pathStorage,namePath, currentName, newFileName, dirName);
                              })
                             .catch((error)=>{
                                  this.debugstring+=" Error en la creacion de Directorio 'files'  ** "+error.code+' / '+error.message+" **";
                                  console.log("-- Error en creacion de Directorio 'files ' "+error.code+' / '+error.message+" **");
                              });

                    });

    }

    makeCopy(namePath, currentName, newFileName, dirName){
        this.debugstring+='## namePath:'+namePath;
        this.debugstring+='## cordova.file.dataDirectory+dirName:'+this.pathStorage+dirName+'/';
        this.debugstring+='## newFileName:'+newFileName;

        console.log("Directorio destino para imagenes: "+this.pathStorage+dirName);

        this.file.copyFile(namePath, currentName, this.pathStorage+dirName+'/', newFileName).then(success => {
          this.lastImage = newFileName;
          this.file.removeFile(namePath,currentName);

        }, error => {
          this.presentToast('Error al almacenar la imagen.');
          this.debugstring+=" | "+ this.pathStorage+dirName+'/'+newFileName;
        });
    }

    prepareCopy(basePath,namePath, currentName, newFileName, dirName){
        this.file.checkDir(basePath,dirName)
                    .then((success)=>{
                            this.debugstring+="-- Directorio existente para viaje: "+dirName;
                            console.log("-- Directorio existente para viaje: "+dirName);
                            this.makeCopy(namePath, currentName, newFileName, dirName);
                         })
                    .catch((err) =>{
                            var replace=false;
                            this.file.createDir(basePath,dirName,replace)
                                   .then((sucess)=>{
                                        this.debugstring+="-- Directorio creado --";
                                        console.log("-- Directorio creado para viaje: "+dirName);
                                        this.makeCopy(namePath, currentName, newFileName, dirName);
                                    })
                                   .catch((error)=>{
                                        this.debugstring+=" Error en la creacion de Directorio "+basePath+dirName+'/'+" ** "+error.code+' / '+error.message+" **";
                                        console.log("-- Error en creacion de Directorio para viaje: "+dirName);
                                    });
                    });
    }

    private presentToast(text) {
        let toast = this.toastCtrl.create({
          message: text,
          duration: 3000,
          position: 'top'
        });
        toast.present();
    }

    // Always get the accurate path to your apps folder
    public pathForImage(img) {
        if (img === null) {
          return '';
        } else {
          return this.pathStorage+this.dirFotos+'/'+img;
        }
    }

    public uploadImage(filename) {
      // Destination URL
      var url = "http://ws.misitio.com/upload";

      // File for Upload
      var targetPath = this.pathForImage(filename);

      // File name only

      var options = {
          fileKey: "file",
          fileName: filename,
          chunkedMode: false,
          httpMethod: "POST",
          mimeType: "multipart/form-data",

          params : {'filename': filename,
                     'id_usuario': this.id_usuario,
                     'token':this.token,
                     'latitude':this.latitude,
                     'longitude':this.longitude,
                     'nota':this.nota
                  },

          headers : {'filename': filename,
                     'id_usuario': this.id_usuario,
                     'x-access-token':this.token,
                     'latitude':this.latitude,
                     'longitude':this.longitude,
                     'nota':this.nota
                  }

      };

      const fileTransfer: TransferObject = this.transfer.create();

      this.loading = this.loadingCtrl.create({
        content: 'Enviando...',
      });
      this.loading.present();

      // Use the FileTransfer to upload the image
      fileTransfer.upload(targetPath, url, options).then(data => {
          this.loading.dismissAll()
          this.presentToast('Imagen enviada.');
      }, err => {
          this.loading.dismissAll()
          this.presentToast('Error al intentar enviar la imagen.');
      });
    }

}
