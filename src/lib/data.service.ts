import { Injectable }      from '@angular/core';
import { Http }            from '@angular/http';
// import { Headers }         from '@angular/http'; // Quitar comentario cuando se habilite el webservice, ver abajo
import { Events,Platform } from 'ionic-angular';
//import { LocationTracker } from '../providers/location-tracker';

//import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DataService {

    public SIGNUP_URL: string;
    usuario:string="";
    nombre:string="";
    email:string="";
    id_usuario:string="";
    loggedIn:boolean = false;
    message:string ="";
    token:string="";
    success:boolean = false;

    static get parameters() {
        return [[Http]];
    }

    constructor(public http:Http,
                public platform: Platform,
                //public locationTracker: LocationTracker,
                public events: Events) {

                  console.log("Iniciando el DataService");

    
                  this.SIGNUP_URL  = "http://ws.misitio.com/authenticate";
    
    }

    isLoggedIn():boolean{
        this.loggedIn = window.localStorage.getItem('loggedin')=='true';

        if (typeof(this.loggedIn)=='undefined' || this.loggedIn==null ){
           window.localStorage.setItem('loggedin','false');
           this.loggedIn = false;
        }

        if(this.loggedIn){
          this.restoreSession();
        }

        // quitar estos comentarios de startTracking() y stopTracking() solo 
        // al compilar para versión android o iOS. Esta funcion de background 
        // no funciona sobre el browser

        // this.locationTracker.startTracking();

        return this.loggedIn;
    }

    logout(){
        // this.locationTracker.stopTracking();
        this.clearSession();
    }

    login(username,password) {

        // Quitar comentario a estas dos lineas, cuando se habilite la invocacion al webservice
        //var body = "user=" + encodeURIComponent(username) + "&pass=" + encodeURIComponent(password);
        //var headers = new Headers();

        console.log("Loggin in..");

        // headers.append('Content-Type', 'application/x-www-form-urlencoded');

        var self = this;

        return new Promise((resolve,reject) =>{

                /* Habilitar un servicio del lado del backEnd para autenticar
                this.http.post(this.SIGNUP_URL, body, {headers: headers})
                         .map(res => res.json())
                         .subscribe( (data) =>  {
                              console.log(data);
                              if (data.success){
                                  self.email       = username;
                                  self.message     = data.message;
                                  self.id_usuario = data.id_usuario;
                                  self.nombre      = data.nombre;
                                  self.token       = data.token;
                                  self.success     = data.success;
                                  self.loggedIn    = true;
                                  self.saveSession();
                                  resolve(self.loggedIn);
                              } else {
                                  self.success     = data.success;
                                  self.message     = data.message
                                  self.loggedIn    = false;

                                  reject(new Error(this.message));
                              }
                        });
                  });
                 */

                        // Mientras tanto simular el acceso
                self.loggedIn    = true;
                self.email       = 'dummy';
                self.message     = 'Usuario y contraseña no validos: tiene que ser "usuario" y "pass"';
                self.id_usuario  = '1';
                self.nombre      = 'Nombre';
                self.token       = 'EFGUIYTR4567898765HGHJJHGH';
                self.success     = true;
                self.loggedIn    = true;
                self.saveSession();

                if (username=="usuario" && password=="pass"){
                    resolve(self.loggedIn);   
                } else {
                  reject(new Error(this.message));
                }

                
          });               

    }

    saveSession() {
      window.localStorage.setItem('loggedin', 'true');
      window.localStorage.setItem('nombre',this.nombre);
      window.localStorage.setItem('id_usuario',this.id_usuario);
      window.localStorage.setItem('email',this.email);
      window.localStorage.setItem('token',this.token);      
    };

    clearSession(){

      this.email       = "";
      this.message     = "";
      this.id_usuario = "";
      this.nombre      = "";
      this.token       = "";
      this.success     = false;
      this.loggedIn    = false;

      window.localStorage.setItem('loggedin','false');
      window.localStorage.removeItem('nombre');
      window.localStorage.removeItem('id_usuario');
      window.localStorage.removeItem('email');
      window.localStorage.removeItem('token');
    };

    restoreSession(){
      this.nombre = window.localStorage.getItem('nombre');
      this.id_usuario=window.localStorage.getItem('id_usuario');
      this.email=window.localStorage.getItem('email');
      this.token=window.localStorage.getItem('token');
    };

    logError(err) {
        console.log(err);
    };

}

