
## Agregar el plugin de cordova para GeoLocalizacion, el servicio queda acoplado
## al componente Provider de nombre DataService en ./src/lib/data.service.ts

### Agregar plugin statusbar

	ionic cordova plugin add cordova-plugin-statusbar

### Para incorporar GeoLocalizacion

	ionic cordova plugin add cordova-plugin-geolocation --variable GEOLOCATION_USAGE_DESCRIPTION="Para demostracion de geo-localizacion"

	npm install --save @ionic-native/geolocation

### Para incorporar GeoLocalizacion en BackGround, es decir, sin necesidad de que esté la App Activa

	ionic cordova plugin add cordova-plugin-mauron85-background-geolocation

	npm install --save @ionic-native/background-geolocation

### Agregar plugin para uso de la camara

	npm install --save @ionic-native/camera  

### Para el manejo y "upload" de imagenes

	ionic cordova plugin add com-sarriaroman-photoviewer
	ionic cordova plugin add @ionic-native/photo-viewer
	npm install --save @ionic-native/file @ionic-native/transfer @ionic-native/file-path